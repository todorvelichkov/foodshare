<?php
namespace Meal\Entity;

use Doctrine\ORM\Mapping as ORM;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

/**
* @ORM\Entity
* @ORM\Table(name="meal_images")
*/
class Image implements InputFilterAwareInterface
{
	protected $inputFilter;
	
	/**
	 * @ORM\Id
	 * @ORM\Column(name="img_id", type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	* @ORM\ManyToOne(targetEntity="Meal", inversedBy="Image")
	* @ORM\JoinColumn(name="meal_id", referencedColumnName="meal_id")
	*/
    protected $meal;
	
	/**
	 * @ORM\Column(name="img_path", type="text")
	 */
	protected $path;
	
	public function getId()
	{
		return $this->id;
	}
	
	public function getMeal()
	{
		return $this->meal;
	}
	
	public function getPath()
	{
		return $this->path;
	}
	
	public function setMeal($meal)
	{
		$this->meal = $meal;
	}	
	
	public function setPath($path)
	{
		$this->path = $path;
	}
	
	/**
	 * Convert the Entity object to an array.
	 *
	 * @return array
	 */
	public function getArrayCopy()
	{
		return get_object_vars($this);
	}
	
	/**
	 * Populate Entity from an array coming from the form.
	 *
	 * @param array $data
	 */
	public function populate($data = array())
	{
		foreach ($data as $key => $val) {
			$this->$key = $val;
		}                
		/*$this->meal_id = $data['meal_id'];
		$this->meal_title = $data['meal_title'];
		$this->meal_description = $data['meal_description'];
		$this->meal_date = $data['meal_date'];
		$this->meal_location = $data['meal_location'];
		$this->meal_slug = $data['meal_slug'];
		$this->meal_keywords = $data['meal_keywords'];
		$this->meal_activity = $data['meal_activity'];*/
	}

	public function setInputFilter(InputFilterInterface $inputFilter)
	{
		throw new \Exception("Not used");
	}        
	
	public function getInputFilter()
	{
		if (!$this->inputFilter) {
			$inputFilter = new InputFilter();

			$factory = new InputFactory();

			$this->inputFilter = $inputFilter;
		}

		return $this->inputFilter;
	}
}