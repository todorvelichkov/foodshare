<?php
namespace Meal\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Meal\Entity\Image;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

/**
* @ORM\Entity
* @ORM\Table(name="meals")
*/
class Meal implements InputFilterAwareInterface
{
	protected $inputFilter;
	
	/**
	 * @ORM\Id
	 * @ORM\Column(name="user_id", type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @ORM\Column(name="meal_title", type="string")
	 */
	protected $title;                
	
	/**
	 * @ORM\Column(name="meal_description", type="text")
	 */
	protected $description;

	/**
	 * @ORM\Column(name="meal_date", type="datetime")
	 */
	protected $date;
	
	/**
	 * @ORM\Column(name="meal_location", type="string")
	 */
	protected $location;
	
	/**
	 * @ORM\OneToOne(targetEntity="CsnUser\Entity\User")
	 * @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
	 */
	protected $chef;
	
	/**
     * @ORM\ManyToOne(targetEntity="Meal\Entity\Type")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="type_id")
	 */
	protected $type;

    /**
     * @ORM\OneToMany(targetEntity="Meal\Entity\Image", mappedBy="Meal")
     */
    protected $images;

	public function __construct()
	{
		$this->images = new ArrayCollection();
	}
	
	public function getId()
	{
		return $this->id;
	}
	
	public function getTitle()
	{
		return $this->title;
	}
	
	public function getDescription()
	{
		return $this->description;
	}
	
	public function getDate()
	{
		return $this->date;
	}
	
	public function getLocation()
	{
		return $this->location;
	}
	
	public function getChef()
	{
		return $this->chef;
	}
	
	public function getType()
	{
		return $this->type;
	}
	
	public function getImages()
	{
		return $this->images;
	}
	
	public function setTitle($title)
	{
		$this->title = $title;
	}
	
	public function setDescription($description)
	{
		$this->description = $description;
	}
	
	public function setDate($date)
	{
		$this->date = $date;
	}
	
	public function setLocation($location)
	{
		$this->location = $location;
	}
	
	public function setChef($chef)
	{
		$this->chef = $chef;
	}
	
	public function setType($type)
	{
		$this->type = $type;
	}
	
	public function setImages(Collection $images)
	{
		foreach ($images as $image) {
			$this->addImage($image);
		}
	}
	
    public function addImage(Image $image)
    {
		if (!$this->images->contains($image)) {
			$image->setMeal($this);
			$this->images[] = $image;
		}
    }

    public function removeImage(Image $image)
    {
        $this->images->remove($image);
    }
	
	/**
	 * Convert the Entity object to an array.
	 *
	 * @return array
	 */
	public function getArrayCopy()
	{
		return get_object_vars($this);
	}
	
	/**
	 * Populate Entity from an array coming from the form.
	 *
	 * @param array $data
	 */
	public function populate($data = array())
	{
		foreach ($data as $key => $val) {
				$this->$key = $val;
		}                
		/*$this->meal_id = $data['meal_id'];
		$this->meal_title = $data['meal_title'];
		$this->meal_description = $data['meal_description'];
		$this->meal_date = $data['meal_date'];
		$this->meal_location = $data['meal_location'];
		$this->meal_slug = $data['meal_slug'];
		$this->meal_keywords = $data['meal_keywords'];
		$this->meal_activity = $data['meal_activity'];*/
	}

	public function setInputFilter(InputFilterInterface $inputFilter)
	{
		throw new \Exception("Not used");
	}        
	
	public function getInputFilter()
	{
		if (!$this->inputFilter) {
			$inputFilter = new InputFilter();

			$factory = new InputFactory();

			$inputFilter->add($factory->createInput(array(
					'name' => 'meal_id',
					'required' => true,
					'filters' => array(
							array('name' => 'Int'),
					),
			)));

			$inputFilter->add($factory->createInput(array(
					'name' => 'meal_title',
					'required' => true,
			)));
	 
			$inputFilter->add($factory->createInput(array(
					'name' => 'meal_description',
					'required' => true,
			)));

			$inputFilter->add($factory->createInput(array(
					'name' => 'meal_date',
					'required' => true,
			)));
			 
			$inputFilter->add($factory->createInput(array(
					'name' => 'meal_location',
					'required' => false,
			)));
	
			$this->inputFilter = $inputFilter;
		}

		return $this->inputFilter;
	}
}